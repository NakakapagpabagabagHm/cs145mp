# My Connection Class
import threading

class Connection(threading.Thread):
	def __init__(self, conn, addr, server):
		threading.Thread.__init__(self)

		#Initialize conn, addr, and server
		self.conn = conn
		self.addr = addr
		self.server = server
		self.running = True

		#Ship Instance
		self.instance = None
		self.resetControls()

	def resetControls(self):
		#Motion Keys
		self.k_w = False
		self.k_s = False
		self.k_q = False
		self.k_e = False
		self.k_space = False

	def returnKeys(self):
		return [self.k_w, self.k_s, self.k_q, self.k_e, self.k_space]

	def run(self):
		while self.running:
			data = self.conn.recv(1024)
			if not data:
				break

			if self.instance == None:
				print 'ERROR: No Instance Assigned'
			else:
				#Attach Instance
				message = str(self.instance) + " " + str(data)
				
				#Parse Message
				message_list = message.split("\n")
				message_list = message_list[0].split(' ')

				#Convert to True or False
				if len(message_list) == 11:
					for i in xrange(0,len(message_list)):
						if message_list[i] == 'True':
							message_list[i] = True
						else:
							message_list[i] = False

					#Assign Keys
					self.k_w = message_list[2]
					self.k_s = message_list[4]
					self.k_q = message_list[6]
					self.k_e = message_list[8]
					self.k_space = message_list[10]
