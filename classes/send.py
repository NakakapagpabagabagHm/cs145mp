#Send thread
import pygame
import ConfigParser
from pygame.locals import *
import threading, os
from time import time
from random import randint
from ship import *
from planet import *
from UI import *
from titleSprite import *
from receive import *

class Send(threading.Thread):
	def __init__(self, socket, screen):
		threading.Thread.__init__(self)
		#End Game
		self.ongoing = True
		self.lead = []

		#Receive Thread
		self.receive = None

		#Initialize Input Values
		self.socket = socket
		self.screen = screen
		self.running = False
		self.time = 0.00

		#Background Initialization
		self.bg = pygame.Surface((800, 600))
		self.bg = self.bg.convert()
		self.bg.fill((255,255,255))
		self.image = pygame.image.load('resources/background.png').convert_alpha()
		self.imageRect = self.image.get_rect()

		#Initialize Sprite Group
		self.group = pygame.sprite.Group()

		#Initialize Ship List
		self.ships = []

		#Initialize Planet List
		self.planets = []

		#Initialize Orbit List
		self.orbits = []
		
		#Initialize Orbit Group
		self.orbitGroup = pygame.sprite.Group()

		#Initialize Bullet List and Group
		self.bullets = []
		self.bulletGroup = pygame.sprite.Group()

		#Initialize Timer
		self.timer = timer()
		self.group.add(self.timer)

		#Initialize Kill Message List
		self.killMessages = killMessages()

		#Initialize Running Values
		self.running = True
		self.resetControls()
		self.time = time()

		#Debug Key
		self.k_g = False

	def run(self):

		while self.running:
		
			#Debug
			if self.k_g:
				for i in self.planets:
					if i.ownership == None:
						print 'planet', i.name , i.instance, "ownership: None"
					else:
						print 'planet',	i.name, i.instance, "ownership:", i.ownership.color		 
				raw_input()


			if self.ongoing == False:
				self.running = False
				rankings = Rankings(self.socket, self.screen, self.lead)
				rankings.start()
				return


			#Controls: Ship Movement
			#if time() - self.time >= 1.00/60:
			self.checkEvents()
			message = "k_w " + str(self.k_w) + " k_s " + str(self.k_s) + " k_q " + str(self.k_q) + " k_e " + str(self.k_e) + " k_space " + str(self.k_space) + "\n"
			self.socket.sendall(message)
			#	self.time = time()

			#Draw Big Planet Orbits
			for i in self.planets:
				collided = False
				for j in self.ships:
					if pygame.sprite.collide_circle(i.orbit, j):
						collided = True
				i.orbit.drawOrbit(collided)
			#for i in self.ships:
			#	for j in self.planets:
			#		collided = False
			#		if pygame.sprite.collide_circle(i, j.orbit):
			#			collided = True						
			#		j.orbit.drawOrbit(collided)

			#Draw Background
			self.screen.blit(self.bg, (0,0))
			self.screen.blit(self.image, self.imageRect)
			
			#Update Sprites
			self.killMessages.updateMessages()
			self.orbitGroup.update()		
			self.bulletGroup.update()	
			self.group.update()			
			
			#Draw Groups
			self.orbitGroup.draw(self.screen)
			self.bulletGroup.draw(self.screen)
			self.group.draw(self.screen)	

			#Update Screen
			pygame.display.update()

	def resetControls(self):
		#Motion Keys
		self.k_w = False
		self.k_s = False
		self.k_q = False
		self.k_e = False
		self.k_space = False

	def checkEvents(self):

		for event in pygame.event.get():
			if event.type == QUIT:
				self.running = False
				os._exit(1)
				return

			if event.type == KEYDOWN:
				if event.key == K_ESCAPE:
					self.running = False
					return

				# A Ship Controls
				# w to accelerate
				if event.key == K_w:
					self.k_w = True
				else:
					self.k_w = False

				# s to deccelerate
				if event.key == K_s:
					self.k_s = True
				else:
					self.k_s = False 

				# q to rotate counterclockwise
				if event.key == K_q:
					self.k_q = True

				# e to rotate clockwise
				if event.key == K_e:
					self.k_e = True

				# space to shoot
				if event.key == K_SPACE:
					self.k_space = True

				#Debug
				if event.key == K_g:
					self.k_g = True

			if event.type == KEYUP:
				if event.key == K_q:
					self.k_q = False

				if event.key == K_e:
					self.k_e = False

				if event.key == K_SPACE:
					self.k_space = False

				if event.key == K_g:
					self.k_g = False



class Rankings:
	def __init__(self, socket, screen, names): #pass ship list here somewhere
		self.socket = socket
		self.screen = screen
		self.bg = pygame.Surface((800, 600))
		self.bg = self.bg.convert()
		self.bg.fill((255,255,255))
		self.image = pygame.image.load('resources/leaderboards.png').convert_alpha()
		self.imageRect = self.image.get_rect()
		self.running = True
		self.firstplace = pygame.image.load('resources/firstplace.png').convert_alpha()
		self.secondplace = pygame.image.load('resources/secondplace.png').convert_alpha()
		self.thirdplace = pygame.image.load('resources/thirdplace.png').convert_alpha()
		self.font = pygame.font.Font('resources/pixel.ttf',35)
		self.names = names

	def checkEvents(self):
		for event in pygame.event.get():
			if event.type == QUIT:
				self.running = False
				os._exit(1)
				return
					
			if event.type == KEYDOWN:
				if event.key == K_ESCAPE:
					
					self.running = False
					textmenu = TextMenu(socket.socket(socket.AF_INET, socket.SOCK_STREAM), self.screen)
					textmenu.start()
					return

	def start(self):
		while(self.running):
			#Check Events
			self.checkEvents()
			
			#Background
			self.screen.blit(self.bg, (0,0))
			self.screen.blit(self.image, self.imageRect)
			
			for i in range(0,len(self.names)):
				if i == 0:
					self.screen.blit(self.firstplace, (27,102))
					self.screen.blit(self.font.render(self.names[i],0,(255,255,255)),(220, 132))
				elif i == 1:
					self.screen.blit(self.secondplace, (27,208))
					self.screen.blit(self.font.render(self.names[i],0,(255,255,255)),(220, 238))
				elif i == 2:
					self.screen.blit(self.thirdplace, (27,315))
					self.screen.blit(self.font.render(self.names[i],0,(255,255,255)),(220, 345))
				elif i == 3:
					self.screen.blit(self.font.render("4",0,(255,255,255)),(63, 430))
					self.screen.blit(self.font.render(self.names[i],0,(255,255,255)),(220, 430))
				elif i == 4:
					self.screen.blit(self.font.render("5",0,(255,255,255)),(63, 490))
					self.screen.blit(self.font.render(self.names[i],0,(255,255,255)),(220, 490))
		
			pygame.display.update()


class TextMenu:
	def __init__(self, socket, screen):
		self.caretPos = 1
		self.screen = screen
		self.socket = socket
		self.bg = pygame.Surface((800, 600))
		self.bg = self.bg.convert()
		self.bg.fill((255,255,255))
		self.image = pygame.image.load('resources/mainmenu.png').convert_alpha()
		self.imageRect = self.image.get_rect()
		self.caret = pygame.image.load('resources/caret.png').convert_alpha()
		self.running = True
		self.spriteGroup = pygame.sprite.Group()
		self.titleSpriteGroup = pygame.sprite.Group()
		a_titleSprite = titleSprite()
		a_titleOrbit = titleOrbit(-150,250,1)
		b_titleOrbit = titleOrbit(900,250,-1)
		a_obstacle = titleFloat(-10,400)
		a_asteroid = titleFly()
		b_asteroid = titleFly()
		self.titleSpriteGroup.add(a_titleSprite)
		self.spriteGroup.add(a_titleOrbit)
		self.spriteGroup.add(b_titleOrbit)
		self.spriteGroup.add(a_obstacle)
		self.spriteGroup.add(a_asteroid)
		self.spriteGroup.add(b_asteroid)
		
		self.orbitSpriteGroup = pygame.sprite.Group()
		#set defaults here??

		config = ConfigParser.ConfigParser()
		config.read('settings.cfg')
		self.ipAddress = config.get('Playerdata', 'ip')
		self.port = config.get('Playerdata', 'port')
		self.name = config.get('Playerdata', 'name')
		self.skin = config.get('Playerdata', 'skin')
		self.color = config.get('Playerdata', 'color')
		
	def checkEvents(self):
		for event in pygame.event.get():
			if event.type == QUIT:
				self.running = False
				os._exit(1)
				return

			if event.type == KEYDOWN:
				if event.key == K_DOWN:
					if self.caretPos < 4:
						self.caretPos += 1
					else:
						self.caretPos = 1
				if event.key == K_UP:
					if self.caretPos > 1:
						self.caretPos -= 1
					else:
						self.caretPos = 4
						
				if event.key == K_RETURN:
					if self.caretPos == 1:
						####START GAME####
						#game = Game(self.screen)
						#game.start()
						self.running = False	
						self.socket.connect((self.ipAddress, int(self.port)))
						send = Send(self.socket, self.screen)
						send.start()
						receive = Receive(self.socket)
						receive.start()
						receive.send = send
						send.receive = receive
					elif self.caretPos == 2:
						self.sett()
					elif self.caretPos == 3:
						self.help()
					elif self.caretPos == 4:
						self.running = False
						os._exit(1)
						return

			# if event.type == KEYUP:
	# Help Menu
	def help(self):
		self.pimage = pygame.image.load('resources/help.png')
		self.pimageRect = self.pimage.get_rect()
		while(True):
			for event in pygame.event.get():
				if event.type == QUIT:
					self.running = False
					os._exit(1)
					return
					
				if event.type == KEYDOWN:
					if event.key == K_ESCAPE:
						return
							
			self.checkEvents()
			self.screen.blit(self.pimage, self.pimageRect)
			pygame.display.flip()
	# Settings Menu
	def sett(self):
		config = ConfigParser.RawConfigParser()
		
		self.pimage = pygame.image.load('resources/settings.png')
		self.pimageRect = self.pimage.get_rect()
		self.hcaretPos = 1
		self.font = pygame.font.Font('resources/pixel.ttf',35)
		while(True):
			
			for event in pygame.event.get():
				if event.type == QUIT:
					self.running = False
					os._exit(1)
					return
				if event.type == KEYDOWN:
					# print event.key
					if event.key == K_ESCAPE:
						config.add_section('Playerdata')
						config.set('Playerdata','IP',self.ipAddress)
						config.set('Playerdata','Port',self.port)
						config.set('Playerdata','Name',self.name)
						config.set('Playerdata','Skin',self.skin)
						config.set('Playerdata','Color',self.color)
						with open('settings.cfg','wb') as configfile:
							config.write(configfile)
						return				
						
					if event.key == K_DOWN:
						if self.hcaretPos < 5:
							self.hcaretPos += 1
						else:
							self.hcaretPos = 1
							
					if event.key == K_UP:
						if self.hcaretPos > 1:
							self.hcaretPos -= 1
						else:
							self.hcaretPos = 5
					
					if event.key == K_BACKSPACE:
						if self.hcaretPos == 1:
							self.ipAddress = self.ipAddress[:-1]
						elif self.hcaretPos == 2:
							self.port = self.port[:-1]
						elif self.hcaretPos == 3:
							self.name = self.name[:-1]
						elif self.hcaretPos == 4:
							self.skin = self.skin[:-1]
						elif self.hcaretPos == 5:				
							self.color = self.color[:-1]
							
					if event.key >= 97 and event.key <= 122 or event.key >= 48 and event.key <= 57 or event.key == 46 or event.key == 44:
						if self.hcaretPos == 1:
							self.ipAddress += chr(event.key)
						elif self.hcaretPos == 2:
							self.port += chr(event.key)
						elif self.hcaretPos == 3:
							self.name += chr(event.key)
						elif self.hcaretPos == 4:
							self.skin += chr(event.key)
						elif self.hcaretPos == 5:				
							self.color += chr(event.key)
						
			self.checkEvents()
			self.screen.blit(self.pimage, self.pimageRect)
			self.screen.blit(self.font.render(self.ipAddress,0,(255,255,255)),(150, 172))
			self.screen.blit(self.font.render(self.port,0,(255,255,255)),(190, 226))
			self.screen.blit(self.font.render(self.name,0,(255,255,255)),(215, 280))
			self.screen.blit(self.font.render(self.skin,0,(255,255,255)),(425, 388))
			self.screen.blit(self.font.render(self.color,0,(255,255,255)),(370, 442))
			
			if self.hcaretPos == 1:
				self.screen.blit(self.caret,(40,166))
			elif self.hcaretPos == 2:
				self.screen.blit(self.caret,(40,220))
			elif self.hcaretPos == 3:
				self.screen.blit(self.caret,(40,274))
			elif self.hcaretPos == 4:
				self.screen.blit(self.caret,(40,382))
			elif self.hcaretPos == 5:
				self.screen.blit(self.caret,(40,436))

			pygame.display.flip()
			
			
	# Start
	def start(self):
		while(self.running):
			#Check Events
			self.checkEvents()
			
			#Background
			self.screen.blit(self.bg, (0,0))
			self.screen.blit(self.image, self.imageRect)
			if self.caretPos == 1:
				self.screen.blit(self.caret,(23,353))
			elif self.caretPos == 2:
				self.screen.blit(self.caret,(58,407))
			elif self.caretPos == 3:
				self.screen.blit(self.caret,(95,461))
			elif self.caretPos == 4:
				self.screen.blit(self.caret,(131,515))
			#Updates
			self.titleSpriteGroup.update()
			self.spriteGroup.update()
			self.orbitSpriteGroup.update()
			
			#Render
			self.spriteGroup.draw(self.screen)
			self.titleSpriteGroup.draw(self.screen)
			self.orbitSpriteGroup.draw(self.screen)
			
			#Update Screen
			pygame.display.update()

class MainMenu:
	def __init__(self, socket, screen):
		self.screen = screen
		self.socket = socket
		self.bg = pygame.Surface((800, 600))
		self.bg = self.bg.convert()
		self.bg.fill((255,255,255))
		self.image = pygame.image.load('resources/background.png').convert_alpha()
		self.imageRect = self.image.get_rect()

		self.running = True

		self.spriteGroup = pygame.sprite.Group()
		self.titleSpriteGroup = pygame.sprite.Group()
		a_titleSprite = titleSprite()
		a_titleOrbit = titleOrbit(-150,250,1)
		b_titleOrbit = titleOrbit(900,250,-1)
		a_obstacle = titleFloat(-10,400)
		a_asteroid = titleFly()
		b_asteroid = titleFly()
		self.titleSpriteGroup.add(a_titleSprite)
		self.spriteGroup.add(a_titleOrbit)
		self.spriteGroup.add(b_titleOrbit)
		self.spriteGroup.add(a_obstacle)
		self.spriteGroup.add(a_asteroid)
		self.spriteGroup.add(b_asteroid)

		self.a_ship = ship(400-10,280)
		self.a_ship.mainMenuInit()
		self.spriteGroup.add(self.a_ship)
		self.resetControls()

		self.orbitSpriteGroup = pygame.sprite.Group()
		self.earthPlanet = earthPlanet(400-37,400,0,1)
		self.orbitSpriteGroup.add(self.earthPlanet.orbit)
		self.orbitSpriteGroup.add(self.earthPlanet.s_orbit)
		self.spriteGroup.add(self.earthPlanet)
		
	def checkEvents(self):
		for event in pygame.event.get():
			if event.type == QUIT:
				self.running = False
				os._exit(1)
				return

			if event.type == KEYDOWN:

				# w to accelerate
				if event.key == K_w:
					self.k_w = True
				else:
					self.k_w = False

				# s to deccelerate
				if event.key == K_s:
					self.k_s = True
				else:
					self.k_s = False 

				# q to rotate counterclockwise
				if event.key == K_q:
					self.k_q = True

				# e to rotate clockwise
				if event.key == K_e:
					self.k_e = True

			if event.type == KEYUP:
				if event.key == K_q:
					self.k_q = False

				if event.key == K_e:
					self.k_e = False

	def start(self):
		while(self.running):
			#Check Events
			self.checkEvents()

			#Ship Movement
			self.a_ship.move(self.k_w, self.k_s, self.k_q, self.k_e)

			#Collision		
	
			#Earth Planet
			if(pygame.sprite.collide_circle(self.earthPlanet.orbit,self.a_ship)):
				if(self.earthPlanet.orbit.shipInOrbit != None):
					pass
				else:
					self.earthPlanet.orbit.initOrbit(self.a_ship)
				self.earthPlanet.orbit.drawOrbit(True)
			else:
				self.earthPlanet.orbit.drawOrbit(False)
			
			#Earth Planet Orbit
			if self.earthPlanet.ownership != None:
				self.running = False
				textmenu = TextMenu(self.socket, self.screen)
				textmenu.start()
				return 

			#Background
			self.screen.blit(self.bg, (0,0))
			self.screen.blit(self.image, self.imageRect)
			
			#Updates
			self.titleSpriteGroup.update()
			self.spriteGroup.update()
			self.orbitSpriteGroup.update()

			#Render
			self.spriteGroup.draw(self.screen)
			self.titleSpriteGroup.draw(self.screen)
			self.orbitSpriteGroup.draw(self.screen)

			#Update Screen
			pygame.display.update()

	def resetControls(self):
		#Motion Keys
		self.k_w = False
		self.k_s = False
		self.k_q = False
		self.k_e = False


