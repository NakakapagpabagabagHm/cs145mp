#Receive Thread
import socket
import threading, os, math
from time import time
from random import randint
from ship import *
from planet import *
from UI import *
from titleSprite import *
from vector import *

class Receive(threading.Thread):
	def __init__(self, socket):
		threading.Thread.__init__(self)

		#Send Thread
		self.send = None

		self.socket = socket
		self.running = False
		self.time = time()

		self.knownShip = []

		#Load Sounds
		self.bulletSound = pygame.mixer.Sound('./resources/Laser_Shoot3.wav')
		self.deathSound = pygame.mixer.Sound('./resources/Explosion4.wav')

	def run(self):
		self.running = True
		while self.running:		
			a_message = self.socket.recv(409600000)
			if a_message == "END":
				self.running = False
				os._exit(1)
				break
			else:
				#Proccess Message
				ln_a_message = a_message.split("\n")

				#print ln_a_message
				#raw_input()
				while len(ln_a_message) > 0 and self.send != None:
					a_message = ln_a_message.pop(0).split(' ')

					#TIME protocol
					if a_message[0] == "SETTIME":
						self.send.timer.currTime = float(a_message[1])

					#END Protocol
					if a_message[0] == "ENDGAME":
						for i in range(1,len(a_message)):											
							self.send.lead.append(a_message[i])
						self.send.ongoing = False
						
					#PLANET Protocol
					if a_message[0] == "PLANET" and len(a_message) >= 15:
					
						#Check if Planet already Exists
						exists = False

						#Check if List is Empty
						if len(self.send.planets) > 0 :
							for i in self.send.planets:
								if i.instance == int(a_message[2]):
									#Set to True
									exists = True

									#Assign Ownership
									if a_message[14] != 'None':
										for j in self.send.ships:
											if j.instance == int(a_message[14]):
												i.ownership = j
												
						#Create Planet if does not Exist
						if exists == False:
							#Create Respective Planet
							tempoPlanet = None
							if a_message[10] == 'ringplanet':							
								tempoPlanet = ringPlanet(int(a_message[4]), int(a_message[6]), int(a_message[2]))
							elif a_message[10] == 'emberplanet':
								tempoPlanet = emberPlanet(int(a_message[4]), int(a_message[6]), int(a_message[2]))
							elif a_message[10] == 'blueplanet':
								tempoPlanet = bluePlanet(int(a_message[4]), int(a_message[6]), int(a_message[2]))
							elif a_message[10] == 'pinkplanet':
								tempoPlanet = pinkPlanet(int(a_message[4]), int(a_message[6]), int(a_message[2]))
							elif a_message[10] == 'earth':
								tempoPlanet = earthPlanet(int(a_message[4]), int(a_message[6]), int(a_message[2]))
							elif a_message[10] == 'mars':
								tempoPlanet = marsPlanet(int(a_message[4]), int(a_message[6]), int(a_message[2]))
							elif a_message[10] == 'station':
								tempoPlanet = stationPlanet(int(a_message[4]), int(a_message[6]), int(a_message[2]))
							elif a_message[10] == 'mixplanet':
								tempoPlanet = mixPlanet(int(a_message[4]), int(a_message[6]), int(a_message[2]))

							#Set Planet Ownership
							if a_message[14] != 'None':
								for i in self.send.ships:
									if i.instance == int(a_message[14]):
										tempoPlanet.ownership = i

							#Add Planet to Group
							self.send.planets.append(tempoPlanet)
							self.send.group.add(tempoPlanet)

							#Add Planet Orbits to Group
							self.send.orbits.append(tempoPlanet.orbit)
							self.send.orbitGroup.add(tempoPlanet.orbit)
							self.send.orbitGroup.add(tempoPlanet.s_orbit)


					#PLANETORBIT Protocol
					if a_message[0] == "PLANETORBIT" and len(a_message) >= 17:
						
						#Search for Orbit in List
						for i in self.send.orbits:

							if i.instance == int(a_message[2]):
								
								#Assign Values
								i.radius = float(a_message[4])
								i.size = int(a_message[6])
								i.totalAngle = float(a_message[8])
								if a_message[10] == "None":
									i.vector = None
								else:
									tempoVector = a_message[10].split(',')
									tempoVector = Vec2d(float(tempoVector[0]), float(tempoVector[1]))
								if a_message[12] == "True":
									i.inOrbit = True
								else:
									i.inOrbit = False
								if a_message[14] == "None":
									i.shipInOrbit = None
								else:
									for j in self.send.ships:
										if j.instance == int(a_message[14]):
											i.shipInOrbit = j
											break
								i.orbitQuota = int(a_message[16])

								break
						
					#SHIP Protocol
					if a_message[0] == "SHIP" and len(a_message) >= 39:
				
						#Check if Ship already Exists				
						exists = False
				
						#Check if List is Empty
						if len(self.send.ships) > 0:			
							for i in self.send.ships:
								if i.instance == int(a_message[2]):
									#Set to True
									exists = True
							
									#Assign Values
								
									#if abs(abs(i.x) - abs(int(float(a_message[10])))) >= 5:								
									i.x = int(float(a_message[10]))

									#if abs(abs(i.y) - abs(int(float(a_message[12])))) >= 5:
									i.y = int(float(a_message[12]))
								
									i.speed = float(a_message[14])
									i.accelerate = float(a_message[16])
									i.strafe = float(a_message[18])
									i.size = int(a_message[20])
									i.angle = float(a_message[22])
									i.d_a = float(a_message[24])

									if i.isAlive and a_message[26] == "False":
										self.deathSound.play()

									if a_message[26] == "True":
										i.isAlive = True
									else:
										i.isAlive = False
									i.deathTime = float(a_message[28])
									i.shootTime = float(a_message[30])
									if a_message[32] == "True":
										i.canShoot = True
									else:
										i.canShoot = False
									i.time = float(a_message[38])

									break

						#Create if Ship does not Exist
						if exists == False:
							color = (int(a_message[34]), int(a_message[35]), int(a_message[36]))
							newShip = ship(int(a_message[6]), int(a_message[8]), color)
							newShip.instance = int(a_message[2])
							newShip.name = str(a_message[4])
							newShip.x = int(float(a_message[10]))
							newShip.y = int(float(a_message[12]))
							newShip.speed = float(a_message[14])
							newShip.accelerate = float(a_message[16])
							newShip.strafe = float(a_message[18])
							newShip.size = int(a_message[20])
							newShip.angle = float(a_message[22])
							newShip.d_a = float(a_message[24])
							if a_message[26] == "True":
								newShip.isAlive = True
							else:
								newShip.isAlive = False
							newShip.deathTime = float(a_message[28])
							newShip.shootTime = float(a_message[30])
							if a_message[32] == "True":
								newShip.canShoot = True
							else:
								newShip.canShoot = False
							newShip.time = float(a_message[38])
	
							#Add Ship to List and Group
							self.send.ships.append(newShip)
							self.send.group.add(newShip)

					#BULLET Protocol
					if a_message[0] == "BULLET" and len(a_message) >= 17:
						
						#Check if Bullet already Exists
						exists = False

						#Search in Bullet List
						for i in self.send.bullets:
							if i.instance == int(a_message[2]):
								
								#Set to True							
								exists = True
								
								#Assign Values
								i.x = int(float(a_message[4]))
								i.y = int(float(a_message[6]))
								i.speed = int(a_message[8])
								i.d_x = float(a_message[10])
								i.d_y = float(a_message[12])
								
								for j in self.send.ships:
									if j.instance == int(a_message[14]):
										i.ownership = j
										break

								i.size = int(a_message[16])

								break

						#Create if Bullet does not Exist
						if exists == False:
							
							#Search for owner Ship
							tempoShip = None
							for i in self.send.ships:
								if i.instance == int(a_message[14]):
									tempoShip = i
									break

							#Create Bullet
							tempoBullet = bullet(int(float(a_message[4])), int(float(a_message[6])), float(a_message[10]), float(a_message[12]), tempoShip, self.send.bullets, int(a_message[2]))

							#Make Bullet Sound
							self.bulletSound.play()
							
							#Add to Bullet List and Group					
							self.send.bullets.append(tempoBullet)
							self.send.bulletGroup.add(tempoBullet)


					#KILLBULLET Protocol
					if a_message[0] == "KILLBULLET" and len(a_message) >= 3:
						
						#Search for Bullet
						for i in self.send.bullets:
							if i.instance == int(a_message[2]):
								
								#Remove from List
								self.send.bullets.remove(i)

								#Kill Bullet
								i.die()
						
								break

					#MESSAGE Protocol
					if a_message[0] == "MESSAGE" and len(a_message) >= 2:

						#Create new MESSAGE
						stringy = ""
						for i in range(1,len(a_message)):
							stringy = stringy + a_message[i] + " "

						newMessage = message(ship(), ship(), stringy)

						#Add to List and Group
						self.send.killMessages.addMessage(newMessage)
						self.send.group.add(newMessage)

					#SUICIDEMESSAGE Protocol
					if a_message[0] == "SUICIDEMESSAGE" and len(a_message) >= 2:
						
						#Create new SUICIDEMESSAGE
						stringy = ""
						for i in range(1,len(a_message)):
							stringy = stringy + a_message[i] + " "

						newMessage = suicideMessage(ship(), stringy)
						
						#Add to List and Group
						self.send.killMessages.addMessage(newMessage)
						self.send.group.add(newMessage)

