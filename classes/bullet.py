import pygame
from pygame.locals import *
from time import time

class bullet(pygame.sprite.Sprite):
	def __init__(self, x, y, d_x, d_y, ownership, group, instance):
		pygame.sprite.Sprite.__init__(self)

		self.x = x
		self.y = y
		self.speed = 1
		self.d_x = d_x
		self.d_y = d_y
		self.ownership = ownership
		self.group = group
		self.size = 4
		self.instance = instance

		self.image = pygame.Surface([4,4])
		self.image.set_colorkey((0, 0, 0), RLEACCEL)

		pygame.draw.circle(self.image, (255, 255, 255), (2,2), 2)

		self.rect = self.image.get_rect()
		self.rect.topleft = self.x, self.y

	def data(self):
		datum = "BULLET instance " + str(self.instance) + " x " + str(self.x) + " y " + str(self.y) + " speed " + str(self.speed) + " d_x " + str(self.d_x) + " d_y " + str(self.d_y) + " ownership " + str(self.ownership.instance) + " size " + str(self.size) + "\n"
		return datum

	def killData(self):
		datum = "KILLBULLET instance " + str(self.instance) + "\n"
		return datum

	def update(self):
		self.x = self.x + self.speed * self.d_x
		self.y = self.y + self.speed * self.d_y

		self.rect = self.image.get_rect()
		self.rect.topleft = self.x, self.y

		#Border movement
		if self.x <= -1*self.size:
			if self.group.count(self) > 0:
				self.group.remove(self)
				self.kill()
		if self.x >= 800 + self.size:
			if self.group.count(self) > 0:
				self.group.remove(self)
				self.kill()

		if self.y <= -1*self.size:
			if self.group.count(self) > 0:
				self.group.remove(self)
				self.kill()
		if self.y >= 600 + self.size:
			if self.group.count(self) > 0:
				self.group.remove(self)
				self.kill()

	def die(self):
		self.kill()

