import pygame
from pygame.locals import *
from time import time
import random

class timer(pygame.sprite.Sprite):
	def __init__(self):
		pygame.sprite.Sprite.__init__(self)
		self.x = 400
		self.y = 20
		self.currTime = 120.00
		self.time = time()
		self.myfont = pygame.font.Font('resources/pixel.ttf', 25)
		self.image = self.myfont.render(str(self.currTime), True, (255,255,255))

		self.rect = self.image.get_rect()
		self.rect.topleft = 400-self.rect.centerx, self.y

	def update(self):
		diff = self.time - time()
		self.currTime = self.currTime - abs(diff)
		self.time = time()

		d_time = str(int(self.currTime/60)) + ':' + str(int(self.currTime%60))
		self.image = self.myfont.render(d_time, True, (255,255,255))

		self.rect = self.image.get_rect()
		self.rect.topleft = 400-self.rect.centerx, self.y


class killMessages:
	def __init__(self):
		self.x = 400
		self.y = 50

		self.messageList = []

	def addMessage(self,message):
		self.messageList.append(message)

	def updateMessages(self):
		for i in self.messageList:
			if i.currTime <= 0:
				self.messageList.remove(i)

		ypos = 50
		for i in self.messageList:
			i.changeXY(i.x, ypos)
			ypos = ypos + 25

class message(pygame.sprite.Sprite):
	def __init__(self, ship1, ship2, message=None):
		pygame.sprite.Sprite.__init__(self)
		messageList = [ 
				ship1.name + " and " + ship2.name + " kissed each others' behinds goodbye!", 
				ship1.name + "'s momma so fat, she bumped into " + ship2.name + "!", 
				ship1.name + " and " + ship2.name + " are now married in heaven.",
				ship2.name + " and " + ship1.name + " exploded into star dust.",
				ship2.name + " orbited too close around " + ship1.name + "'s waist.",
				ship2.name + " and " + ship1.name + " should get a room.",
				ship1.name + " and " + ship2.name + " were hit in the head as babies.",
				ship1.name + " and " + ship2.name + " kissed and said goodbye to this world!",
				ship1.name + " and " + ship2.name + " farted at each other and inhaled the poisonous gas.",
				ship1.name + " thought " + ship2.name + " was his mom.",
				ship1.name + " and " + ship2.name + " forgot how to play the game.",
		]

		self.myfont = pygame.font.Font('resources/pixel.ttf', 10)
		if message == None:
			self.message = random.choice(messageList)
		else:
			self.message = message
		self.image = self.myfont.render(self.message, True, (255,255,255))
		self.currTime = 5.00
		self.time = time()

		self.x = 0
		self.y = 0
		
		self.rect = self.image.get_rect()
		self.rect.topleft = (400-self.rect.centerx),0

	def data(self):
		datum = "MESSAGE " + str(self.message) + "\n"
		return datum

	def changeMessage(self, message):
		self.message = message

	def changeXY(self, x, y):
		self.x = x
		self.y = y
		
		self.rect = self.image.get_rect()
		self.rect.topleft = (400-self.rect.centerx),y

	def update(self):
		diff = abs(self.time - time())
		self.currTime = self.currTime - diff
		self.time = time()
		if self.currTime <= 0.00:
			self.kill()

class suicideMessage(pygame.sprite.Sprite):
	def __init__(self, ship, message=None):
		pygame.sprite.Sprite.__init__(self)
		messageList = [
			ship.name + " committed sudoku!",
			ship.name + " likes big planets.",
			ship.name + " forgot what a big giant rock looks like.",
			ship.name + " forgot his underwear on the wrong planet.",
			ship.name + " sure knows where he's going.",
			ship.name + " married a planet!.",
			ship.name + " must be wondering if he lost a friend somewhere along in the bitterness.",
			ship.name + " knows na where broken hearts go talaga.",
			ship.name + " is now being used for alien experimentation.",
			ship.name + " needs to uninstall this game.",
			ship.name + " is now one with the stars.",
			ship.name + " couldn't tell left from right.",
			ship.name + " tried to tame aliens.",
			ship.name + " thought he was Chuck Norris.",
			ship.name + " didn't buy the no-collisions dlc.",
			ship.name + " forgot the controls",
			"It's a bird, it's a plane, no it's " + ship.name + " and he's dead.",
			ship.name + " did not wear seatbelts.",
			ship.name + " divided by zero.",
		]

		self.myfont = pygame.font.Font('resources/pixel.ttf', 10)
		if message == None:
			self.message = random.choice(messageList)
		else:
			self.message = message
		self.image = self.myfont.render(self.message, True, (255,255,255))
		self.currTime = 5.00
		self.time = time()

		self.x = 0
		self.y = 0
		
		self.rect = self.image.get_rect()
		self.rect.topleft = (400-self.rect.centerx),0

	def data(self):
		datum = "SUICIDEMESSAGE " + str(self.message) + "\n"
		return datum

	def changeMessage(self, message):
		self.message = message

	def changeXY(self, x, y):
		self.x = x
		self.y = y
		
		self.rect = self.image.get_rect()
		self.rect.topleft = (400-self.rect.centerx),y

	def update(self):
		diff = abs(self.time - time())
		self.currTime = self.currTime - diff
		self.time = time()
		if self.currTime <= 0.00:
			self.kill()



