import pygame
from pygame.locals import *
from time import time

class timer(pygame.sprite.Sprite):
	def __init__(self):
		pygame.sprite.Sprite.__init__(self)
		self.x = 400
		self.y = 20
		self.currTime = 120.00
		self.time = time()
		self.myfont = pygame.font.SysFont('monospace', 20)
		self.image = self.myfont.render(str(self.currTime), True, (255,255,255))

		self.rect = self.image.get_rect()
		self.rect.topleft = self.x, self.y

	def update(self):
		diff = self.time - time()
		self.currTime = self.currTime - abs(diff)
		self.time = time()

		d_time = str(int(self.currTime/60)) + ':' + str(int(self.currTime%60))
		self.image = self.myfont.render(d_time, True, (255,255,255))

		self.rect = self.image.get_rect()
		self.rect.topleft = self.x, self.y


