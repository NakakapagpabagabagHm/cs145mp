import socket
import pygame 
import math
from pygame.locals import *
from random import randint
from ship import *
from planet import *
from UI import *
from titleSprite import *
from gamefunctions import *
from connection import *
from gamethread import *

class Server:
	def __init__(self):		
		#State HOST and PORT
		HOST = ''
		PORT = 8888

		#Initialize Socket
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.bind((HOST, PORT))

		#Maximum of 10 Players
		s.listen(10)

		#Initialize Connection List
		connection_list = []

		#Initialize Pygame
		pygame.init()
		SCREEN = pygame.display.set_mode((800, 600))

		#Initialize Game
		game = Game(SCREEN)
		gamethread = GameThread(game)
		gamethread.start()

		#Accept Connection until 20 Players
		for i in xrange(1,20):

			#Accept the Connection
			conn, addr = s.accept()
			print 'Connected to', addr			

			#Create the Connection Class
			a_connection = Connection(conn,addr,self)
			connection_list.append(a_connection)

			#Add Player to Game
			game.createPlayer(a_connection)
		
			#Start the Connection Thread
			a_connection.start()
			

a_server = Server()
