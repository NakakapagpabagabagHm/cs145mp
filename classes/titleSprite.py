import pygame, math, random
from pygame.locals import *

class titleSprite(pygame.sprite.Sprite):
	def __init__(self):
		pygame.sprite.Sprite.__init__(self)
		self.x = 400
		self.y = 300
		self.image = pygame.image.load('resources/uchuusenTitle.png')
		#self.pixel_array = pygame.PixelArray(self.image)
		#0, 169, 199
		#self.pixel_array.replace((255, 255, 255), (0,255,0))
		#self.image = self.pixel_array.make_surface()

		self.sinLoop = 0

		self.rect = self.image.get_rect()
		self.rect.topleft = 400-self.rect.centerx, self.y

	def update(self):
		if(self.sinLoop >= 2*math.pi):
			self.sinLoop = 0
		else:
			self.sinLoop = self.sinLoop + 0.01

		self.y = math.sin(self.sinLoop)
	
		self.rect = self.image.get_rect()
		self.rect.topleft = 400-self.rect.centerx, self.y*20 + 50

class titleOrbit(pygame.sprite.Sprite):
	def __init__(self, x_o, y_o, signum):
		pygame.sprite.Sprite.__init__(self)
		self.x_o = x_o
		self.y_o = y_o
		self.signum = signum
		self.x = 0
		self.y = 0
		self.image = pygame.image.load('resources/cplanet' + str(random.randint(1,8)) + '.png')
		
		self.sinLoop = random.randint(0,2)

		self.rect = self.image.get_rect()
		self.rect.topleft = self.x, self.y

	def update(self):
		if(self.sinLoop >= 2*math.pi):
			self.sinLoop = 0
		else:
			self.sinLoop = self.sinLoop + self.signum * 0.001

		self.x = self.x_o + 250*math.sin(self.sinLoop)
		self.y = self.y_o + 250*math.cos(self.sinLoop)

		self.rect = self.image.get_rect()
		self.rect.topleft = self.x, self.y		

class titleFloat(pygame.sprite.Sprite):
	def __init__(self, x_o, y_o):
		pygame.sprite.Sprite.__init__(self)
		self.x_o = x_o
		self.y_o = y_o
		self.x = 0
		self.y = 0
		ranNum = random.randint(1,2)
		if(ranNum == 1):
			self.image = pygame.image.load('resources/astronaut.png')
		else:
			self.image = pygame.image.load('resources/nyancat.png')
		self.sinLoop = 0

		self.rect = self.image.get_rect()
		self.rect.topleft = self.x, self.y

	def update(self):
		if(self.x >= 810):
			self.x = -10

		self.sinLoop = self.sinLoop + 0.005
		self.x = self.x + 0.1
		self.y = self.y_o + 50*math.sin(0.5*self.sinLoop)

		self.rect = self.image.get_rect()
		self.rect.topleft = self.x, self.y

class titleFly(pygame.sprite.Sprite):
	def __init__(self):
		pygame.sprite.Sprite.__init__(self)
		self.x = -10
		self.y = -1 * random.randint(0,100)
		self.speed = random.randint(0, 5) * 0.1
		self.image = pygame.image.load('resources/asteroid.png')

		self.rect = self.image.get_rect()
		self.rect.topleft = self.x, self.y

	def update(self):
		self.x = self.x + self.speed
		self.y = self.y + self.speed

		if(self.x >= 810 and self. y >= 610):
			ranNum = random.randint(0,800)
			self.x = ranNum
		if(self.y >= 610):
			ranNum = random.randint(10,100)
			self.y = -1 * ranNum

		self.rect = self.image.get_rect()
		self.rect.topleft = self.x, self.y
		

		
