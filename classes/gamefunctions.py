import os, sys
import pygame 
from pygame.locals import *
from random import randint
from ship import *
from planet import *
from UI import *
from titleSprite import *
from time import time

class Game:
	def __init__(self, screen):
		self.screen = screen
		
		self.bg = pygame.Surface((800, 600))
		self.bg = self.bg.convert()
		self.bg.fill((255,255,255))
		self.image = pygame.image.load('resources/background.png').convert_alpha()
		self.imageRect = self.image.get_rect()
		
		self.running = True

		self.group = pygame.sprite.Group()

		#Initialize List of Ships
		self.ships = []

		#Initialize Possible Ship Spawn Positions
		self.positionList = [(2,2), (2,578), (778,578), (778,2), (2,360), (480,578), (778,240), (320,2), (2,240), (320,578), (778,360), (480,2), (2,120), (160,578), (778,480), (160,2), (2,480), (640,578), (778,240), (640,2)]
		self.positionList.reverse()

		#Initialize Possible Ship Colors
		self.colorList = [(217,30,24), (65,131,215), (0,205,0), (247,202,24), (248,148,6), (197,239,247), (191,85,236), (219,10,91), (78,205,196), (135,211,124), (44,62,80), (231,76,60), (174,168,211), (245,215,110), (242,120,75), (108,122,137), (171,183,183), (75,119,190), (200,247,197), (246,71,71)]
		self.colorList.reverse()

		#Initialize Instance Counter for Ships
		self.instanceCounter = 1

		#Initialize Planet List
		self.planets = []

		#Initialize Orbit List
		self.orbits = []
		self.orbitGroup = pygame.sprite.Group()
		
		#Generate Planets		
		self.generatePlanets()

		#Initialize Bullet List and Group
		self.bullets = []
		self.bulletGroup = pygame.sprite.Group()

		#Initalize Bullet Instances
		self.bulletInstances = range(1,10000)
		self.bulletInstances.reverse()

		self.timer = timer()
		self.hasTimer = False
		
		self.killMessages = killMessages()

		self.resetControls()
		self.resetBControls()

		#Ship Send Time
		self.shipTime = time()

		#Debug Key
		self.k_g = False

	def checkEvents(self):
		for event in pygame.event.get():
			if event.type == QUIT:
				self.running = False
				return

			if event.type == KEYDOWN:
				if event.key == K_ESCAPE:
					self.running = False
					return

				# A Ship Controls
				# w to accelerate
				if event.key == K_w:
					self.k_w = True
				else:
					self.k_w = False

				# s to deccelerate
				if event.key == K_s:
					self.k_s = True
				else:
					self.k_s = False 

				# q to rotate counterclockwise
				if event.key == K_q:
					self.k_q = True

				# e to rotate clockwise
				if event.key == K_e:
					self.k_e = True

				# space to shoot
				if event.key == K_SPACE:
					self.k_space = True

				# B Ship Controls
				# i to accelerate
				if event.key == K_i:
					self.k_i = True
				else:
					self.k_i = False

				# k to deccelerate
				if event.key == K_j:
					self.k_j = True
				else:
					self.k_j = False

				# j to rotate counterclockwise
				if event.key == K_k:
					self.k_k = True
				else:
					self.k_k = False

				# l to rotate clockwise
				if event.key == K_l:
					self.k_l = True
				else:
					self.k_l = False

				if event.key == K_g:
					self.k_g = True
				else:
					self.k_g = False

			if event.type == KEYUP:
				if event.key == K_q:
					self.k_q = False

				if event.key == K_e:
					self.k_e = False

				if event.key == K_j:
					self.k_j = False

				if event.key == K_l:
					self.k_l = False

				if event.key == K_g:
					self.k_g = False

				if event.key == K_f:
					self.k_f = False
		
	def start(self):
		while(self.running):
			#Checks Events
			self.checkEvents()
			

			#Start time on first ship
			if(self.hasTimer == False and len(self.ships) == 1 ):			
				self.timer = timer()
				self.group.add(self.timer)
				self.hasTimer = True

			#End the game
			if(self.hasTimer == True and int(math.floor(self.timer.currTime)) == 0):
				#sort ships by rank
				for i in range(len(self.ships)):
					for j in range(len(self.ships) - 1, i, -1):
						if(self.ships[j].captures > self.ships[j-1].captures):
							tmp = self.ships[i]
							self.ships[i] = self.ships[j]
							self.ships[j] = tmp
						elif(self.ships[j].captures == self.ships[j-1].captures):
							if(self.ships[j].kills > self.ships[j-1].kills):
								tmp = self.ships[i]
								self.ships[i] = self.ships[j]
								self.ships[j] = tmp		
							elif(self.ships[j].kills == self.ships[j-1].kills):
								if(self.ships[j].deaths < self.ships[j-1].deaths):
									tmp = self.ships[i]
									self.ships[i] = self.ships[j]
									self.ships[j] = tmp		
								

				alldata = "ENDGAME"
				for i in range(0,min(5, len(self.ships))):
					 alldata = alldata + " " + self.ships[i].name
				alldata = alldata + "\n"

				for i in self.ships:
					i.sendData(alldata)

 				#restart server here
				self.running = False
				os._exit(1)

			#Sending Data
			for i in self.ships:
				alldata = ""

				#Add Planet Data
				for j in self.planets:
					data = j.data()
					alldata = alldata + data

				#Add Orbit Data
				for j in self.orbits:
					data = j.data()
					alldata = alldata + data

				#Add Ship Data				
				for j in self.ships:
					data = j.data()
					alldata = alldata + data
			
				#Add Bullet Data
				for j in self.bullets:
					data = j.data()
					alldata = alldata + data

				if len(self.ships) > 1:
					data = "SETTIME " + str(self.timer.currTime) + "\n"
					alldata = alldata + data
					#new_ship.sendData(newdata)


				i.sendData(alldata)

			#Ship Movement and Shooting
			for i in self.ships:
				commands = i.conn.returnKeys()

				i.move(commands[0], commands[1], commands[2], commands[3])
				
				if commands[4] == True:
					i.shoot(commands[4], self.bulletGroup, self.bullets, self.bulletInstances.pop())

			#Collision
			collide_list = []
			for i in self.ships:

				#Planet Collision
				for j in self.planets:
					if pygame.sprite.collide_circle(i, j):
						i.collide(j)
						self.resetControls()

						newMessage = suicideMessage(i)
						self.killMessages.addMessage(newMessage)
						self.group.add(newMessage)

						#Send Message Data
						for k in self.ships:
							k.sendData(newMessage.data())

				#Ship Collision
				for j in self.ships:
					
					if i.instance != j.instance and pygame.sprite.collide_circle(i,j):
						i.collideShip()
						self.resetControls()
						
						if collide_list.count(j) > 0:
							newMessage = message(i,j)
							self.killMessages.addMessage(newMessage)
							self.group.add(newMessage)

							#Send Message Data
							for k in self.ships:
								k.sendData(newMessage.data())

						
						collide_list.append(i)
						collide_list.append(j)

				#Bullet Collision
				for j in self.bullets:
					if pygame.sprite.collide_circle(i, j) and i != j.ownership and i.isAlive and not i.isInvul:
						self.bullets.remove(j)
						self.bulletInstances.append(j.instance)
					
						#Send Bullet Death Data
						for k in self.ships:
							k.sendData(j.killData())

						j.die()
						i.collideShip()
						j.ownership.kills += 1
						newMessage = message(i, j.ownership)
						self.killMessages.addMessage(newMessage)
						self.group.add(newMessage)
						print 'niyargh'
						#Send Message Data
						print 'j'
						for k in self.ships:
							k.sendData(newMessage.data())

			#Bullet Collision with Planets
			for i in self.bullets:
				for j in self.planets:
					if pygame.sprite.collide_circle(i, j):
						self.bullets.remove(i)
						self.bulletInstances.append(i.instance)

						#Send Bullet Death Data
						for k in self.ships:
							k.sendData(i.killData())

						i.die()

			#Planet Orbits
			for i in self.planets:
				num_orbits = 0
				has_collided = False
				for j in self.ships:
					if pygame.sprite.collide_circle(i.orbit,j):
						has_collided = True
						num_orbits = num_orbits + 1

						#Clean Planet
						if not i.orbit.inOrbit:
							i.orbit.initOrbit(j)
						else:
							#Current Orbiting Ship is Orbiting it
							if i.orbit.shipInOrbit == j:
								pass
							else:
							#Different Ship is currently Orbiting it
								i.orbit.resetOrbit()
								i.orbit.initOrbit(j)

						#If more than one ship in Orbit, force reset orbit
						if num_orbits > 1:
							i.orbit.resetAllOrbit()

					elif i.orbit.shipInOrbit == j:
						i.orbit.resetOrbit()
				
				if not has_collided:
					i.orbit.drawOrbit(False)
				else:
					i.orbit.drawOrbit(True)

			#Debug
			if self.k_g:
				for i in self.planets:
						if i.ownership == None:
							print 'planet', i.name , i.instance, "ownership: None"
						else:
							print 'planet',	i.name, i.instance, "ownership:", i.ownership.color		 
				raw_input()		

			#Replenish Bullet Instances
			self.replenishBullets()		

			#Background
			self.screen.blit(self.bg, (0,0))
			self.screen.blit(self.image, self.imageRect)
			
			#Sprites
			self.killMessages.updateMessages()
			self.orbitGroup.update()		
			self.bulletGroup.update()	
			self.group.update()			
			self.orbitGroup.draw(self.screen)
			self.bulletGroup.draw(self.screen)
			self.group.draw(self.screen)	

			#Update Screen
			pygame.display.update()

	def resetControls(self):
		#Motion Keys
		self.k_w = False
		self.k_s = False
		self.k_q = False
		self.k_e = False
		self.k_space = False

	def resetBControls(self):
		#B Ship Motion Keys
		self.k_i = False
		self.k_j = False
		self.k_k = False
		self.k_l = False

	def replenishBullets(self):
		for i in range(1,10000):
			exists = False		
			for j in self.bullets:
				if j.instance == i:
					exists = True

			if exists == False:
				self.bulletInstances.append(i)

	def generatePlanets(self):
		self.planetCount = 9
		
		tempo_planets = []
		tries = 0

		planet_instance = 1
		while self.planetCount > len(tempo_planets):
			random_planet = randint(0,7)
			if random_planet == 0:
				tempo_planet = ringPlanet(randint(100, 700), randint(100, 500), planet_instance)
			elif random_planet == 1:
				tempo_planet = emberPlanet(randint(100, 700), randint(100, 500), planet_instance)
			elif random_planet == 2:
				tempo_planet = bluePlanet(randint(100, 700), randint(100, 500), planet_instance)		
			elif random_planet == 3:
				tempo_planet = pinkPlanet(randint(100, 700), randint(100, 500), planet_instance)	
			elif random_planet == 4:
				tempo_planet = earthPlanet(randint(100, 700), randint(100, 500), planet_instance)
			elif random_planet == 5:
				tempo_planet = marsPlanet(randint(100, 700), randint(100, 500), planet_instance)	
			elif random_planet == 6:
				tempo_planet = stationPlanet(randint(100, 700), randint(100, 500), planet_instance)	
			elif random_planet == 7:
				tempo_planet = mixPlanet(randint(100, 700), randint(100, 500), planet_instance)	
			tplanet_collide = False
			for i in tempo_planets:
				if pygame.sprite.collide_circle(i.s_orbit, tempo_planet.s_orbit):
					tplanet_collide = True

			if not tplanet_collide:
				tempo_planets.append(tempo_planet)
				tries = 0
				planet_instance = planet_instance + 1
			else:
				tries = tries + 1
				

			if tries == 1000:
				self.planetCount = self.planetCount - 1
				tries = 0

		for i in tempo_planets:
			self.planets.append(i)
			self.group.add(i)
			self.orbits.append(i.orbit)
			self.orbitGroup.add(i.orbit)
			self.orbitGroup.add(i.s_orbit)

	def createPlayer(self, conn):
		#Get Coordinates and Color from List		
		coordinates = self.positionList.pop()
		color = self.colorList.pop()
	
		#Create new Ship
		new_ship = ship(coordinates[0], coordinates[1], color)
		new_ship.instance = self.instanceCounter
		new_ship.addConn(conn)

		#Add to Groups
		self.ships.append(new_ship)
		self.group.add(new_ship)

		#Increment Counter
		self.instanceCounter = self.instanceCounter + 1
		if len(self.ships) > 1:
			newdata = "SETTIME " + str(self.timer.currTime) + "\n"
			print newdata
			new_ship.sendData(newdata)



