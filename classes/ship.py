import pygame, math
import ConfigParser
from pygame.locals import *
from time import time
from bullet import *

class ship(pygame.sprite.Sprite):
	def __init__(self,x_o=0, y_o=0, color=(255,0,0)):
		pygame.sprite.Sprite.__init__(self)
		config = ConfigParser.ConfigParser()
		config.read('settings.cfg')

		self.instance = 1
		self.name = config.get('Playerdata', 'name')
		self.x_o = x_o
		self.y_o = y_o
		self.x = self.x_o
		self.y = self.y_o
		self.speed = 1.00/60
		self.accelerate = 0.00
		self.strafe = 0.00
		self.size = 20
		self.angle = 90.00
		self.d_a = 0.00
		self.isAlive = True
		self.deathTime = 0.00
		self.shootTime = 0.00
		self.canShoot = True
		self.color = color
		self.time = time()
		self.itime = time()
		self.isInvul = True
		self.conn = None
		self.sendTime = time()

		self.image = pygame.image.load('resources/ship1.png').convert_alpha()
		self.original = self.image.copy()
		self.kills = 0
		self.captures = 0
		self.deaths = 0
		#self.image = pygame.transform.rotate(self.image, self.angle)
		self.pixel_array = pygame.PixelArray(self.original)
		self.pixel_array.replace((255, 255, 255), self.color)
		self.image = pygame.transform.rotate(self.image, self.angle)
		self.image = self.pixel_array.make_surface()

		self.rect = self.image.get_rect()
		self.rect.topleft = self.x, self.y

	def move(self, acc=False, decc=False, rot_l=False, rot_r=False):
		if self.isAlive:
			self.y = self.y - math.sin(self.angle*0.0174532925)*self.speed*self.accelerate
			self.x = self.x + math.cos(self.angle*0.0174532925)*self.speed*self.accelerate + self.strafe


			if acc:
				self.accelerate = self.accelerate + 0.1 #0.01

			if decc:
				self.accelerate = self.accelerate - 0.05 #0.005

			if rot_l:
				self.d_a = self.d_a + 0.5


			if rot_r:
				self.d_a = self.d_a - 0.5

			if self.accelerate < 0.00:
				self.accelerate = 0.00

			self.d_a = self.d_a % 360
			self.angle = (self.angle + self.d_a) % 360

			#Border movement
			if self.x <= -1*self.size:
				self.x = 800
			if self.x >= 800 + self.size:
				self.x = 0

			if self.y <= -1*self.size:
				self.y = 600
			if self.y >= 600 + self.size:
				self.y = 0

	def rot_center(self, image, angle):
	    """rotate an image while keeping its center and size"""
	    orig_rect = image.get_rect()
	    rot_image = pygame.transform.rotate(image, angle)
	    rot_rect = orig_rect.copy()
	    rot_rect.center = rot_image.get_rect().center
	    rot_image = rot_image.subsurface(rot_rect).copy()
	    return rot_image	

	def collide(self, planet):
		self.isAlive = False
		self.deaths = self.deaths + 1
		self.deathTime = 3.00
		self.deaths += 1

	def collideShip(self):
		self.isAlive = False
		self.deaths = self.deaths + 1
		self.deathTime = 0.5
		self.deaths += 1

	def changeXY(self,x,y):
		self.x = x
		self.y = y

		self.rect = self.image.get_rect()
		self.rect.topleft = self.x, self.y

	def mainMenuInit(self):
		self.angle = 270.00

	def shoot(self, shooting, group, bullets, instance):
		if self.canShoot and shooting:
			a_bullet = bullet(self.rect.center[0], self.rect.center[1], math.sin((self.angle+90)*0.0174532925)*7, math.cos((self.angle+90)*0.0174532925)*7, self, bullets, instance)
			bullets.append(a_bullet)
			group.add(a_bullet)
			self.canShoot = False
			self.shootTime = 0.1	

	def addConn(self, conn):
		self.conn = conn
		self.conn.instance = self.instance

	def data(self):
		return "SHIP instance " + str(self.instance) + " name " + str(self.name) + " x_o " + str(self.x_o) + " y_o " + str(self.y_o) + " x " + str(self.x) + " y " + str(self.y) + " speed " + str(self.speed) + " accelerate " + str(self.accelerate) + " strafe " + str(self.strafe) + " size " + str(self.size) + " angle " + str(self.angle) + " d_a " + str(self.d_a) + " isAlive " + str(self.isAlive) + " deathTime " + str(self.deathTime) + " shootTime " + str(self.shootTime) + " canShoot " + str(self.canShoot) + " color " + str(self.color[0]) + " " + str(self.color[1]) + " " + str(self.color[2]) + " time " + str(self.time) + "\n"

	def sendData(self, message):
		if self.conn != None:
			self.conn.conn.sendall(message)

	def update(self):
		
		if self.isAlive:
			self.y = self.y - math.sin(self.angle*0.0174532925)*self.speed*self.accelerate
			self.x = self.x + math.cos(self.angle*0.0174532925)*self.speed*self.accelerate + self.strafe

			self.d_a = self.d_a % 360
			self.angle = (self.angle + self.d_a) % 360
			
			#Border movement
			if self.x <= -1*self.size:
				self.x = 800
			if self.x >= 800 + self.size:
				self.x = 0

			if self.y <= -1*self.size:
				self.y = 600
			if self.y >= 600 + self.size:
				self.y = 0


		if(self.isInvul and time() - self.itime >= 5):
			self.isInvul = False
			
		if not self.isAlive and self.deathTime >= 0.00:
			self.deathTime = self.deathTime - (time() - self.time)
			self.image = pygame.Surface([self.size, self.size])
			self.image.set_colorkey((0, 0, 0), RLEACCEL)
			self.x = self.x_o
			self.y = self.y_o

		if not self.isAlive and self.deathTime <= 0.00:
			self.deathTime = 0.00
			self.isAlive = True
			self.x = self.x_o
			self.y = self.y_o
			self.image = self.rot_center(self.original, 90)
			self.accelerate = 0.00
			self.strafe = 0.00
			self.itime = time()
			self.isInvul = True
			
		if not self.canShoot and self.shootTime >= 0.00:
			self.shootTime = self.shootTime - (time() - self.time)

		if not self.canShoot and self.shootTime < 0.00:
			self.canShoot = True
			self.shootTime = 0.00
		
		self.rect = self.image.get_rect()
		self.rect.topleft = self.x, self.y

		self.time = time()

		if self.isAlive:
			self.image = self.rot_center(self.original, self.angle)
		self.d_a = 0.00

		#Send Data to Client
		#if time() - self.sendTime >= 1.00/30:
		#	self.sendData()
		#	self.sendTime = time()
