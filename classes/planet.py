import pygame
from pygame.locals import *
from planetOrbit import *

class planet(pygame.sprite.Sprite):
	def __init__(self, x=100, y=100, size=65, instance=0):
		pygame.sprite.Sprite.__init__(self)
		
		self.instance = instance
		self.x = x
		self.y = y
		self.size = size

		self.name = 'defaultplanet'
		self.radius = self.size/2

		self.orbit = planetOrbit(self,self.x,self.y,self.size)
		self.s_orbit = smallPlanetOrbit(self, self.x, self.y, self.size)
		
		self.ownership = None

		self.image = pygame.Surface([self.size, self.size])
		self.image.set_colorkey((0, 0, 0), RLEACCEL)

		self.rect = self.image.get_rect()
		self.rect.topleft = self.x, self.y
		
		pygame.draw.circle(self.image, (255, 255, 0), (self.size/2, self.size/2), self.radius)

	def data(self):
		datum = "PLANET instance " + str(self.instance) + " x " + str(self.x) + " y " + str(self.y) + " size " + str(self.size) + " name " + str(self.name) + " radius " + str(self.radius) + " ownership "

		if self.ownership == None:
			datum = datum + "None\n"
		else:
			datum = datum + str(self.ownership.instance) + "\n"

		return datum

	def update(self):
		pass

class ringPlanet(planet):
	def __init__(self,x=100,y=100,instance=0):
		planet.__init__(self,x,y,85,instance)
	
		self.name = 'ringplanet'

		self.orbit = planetOrbit(self,self.x+85/2,self.y,self.size)
		self.s_orbit = smallPlanetOrbit(self, self.x+85/2, self.y, self.size)

		self.image = pygame.image.load('resources/cplanet1.png').convert_alpha()

		self.rect = self.image.get_rect()
		self.rect.topleft = self.x, self.y

class emberPlanet(planet):
	def __init__(self,x=100,y=100,instance=0):
		planet.__init__(self,x,y,80,instance)

		self.name = 'emberplanet'

		self.image = pygame.image.load('resources/cplanet2.png').convert_alpha()

		self.rect = self.image.get_rect()
		self.rect.topleft = self.x, self.y

class bluePlanet(planet):
	def __init__(self,x=100,y=100,instance=0):
		planet.__init__(self,x,y,80,instance)

		self.name = 'blueplanet'

		self.image = pygame.image.load('resources/cplanet3.png').convert_alpha()

		self.rect = self.image.get_rect()
		self.rect.topleft = self.x, self.y

class pinkPlanet(planet):
	def __init__(self,x=100,y=100,instance=0):
		planet.__init__(self,x,y,100,instance)

		self.name = 'pinkplanet'
	
		self.image = pygame.image.load('resources/cplanet4.png').convert_alpha()

		self.rect = self.image.get_rect()
		self.rect.topleft = self.x, self.y

class earthPlanet(planet):

	def __init__(self,x=100,y=100,instance=0,z=3):
		planet.__init__(self,x,y,75,instance)

		self.name = 'earth'
		
		self.orbit = planetOrbit(self,x,y,75,False,z)
		self.image = pygame.image.load('resources/cplanet5.png').convert_alpha()
		
		self.rect = self.image.get_rect()
		self.rect.topleft = self.x, self.y
		
class marsPlanet(planet):
	def __init__(self,x=100,y=100,instance=0):
		planet.__init__(self,x,y,65,instance)

		self.name = 'mars'
		
		self.image = pygame.image.load('resources/cplanet6.png').convert_alpha()
		
		self.rect = self.image.get_rect()
		self.rect.topleft = self.x, self.y
		
class stationPlanet(planet):
	def __init__(self,x=100,y=100,instance=0):
		planet.__init__(self,x,y,55,instance)

		self.name = 'station'
		
		self.image = pygame.image.load('resources/cplanet7.png').convert_alpha()
		
		self.rect = self.image.get_rect()
		self.rect.topleft = self.x, self.y
		
class mixPlanet(planet):
	def __init__(self,x=100,y=100,instance=0):
		planet.__init__(self,x,y,70,instance)

		self.name = 'mixplanet'
		
		self.image = pygame.image.load('resources/cplanet8.png').convert_alpha()
		
		self.rect = self.image.get_rect()
		self.rect.topleft = self.x, self.y
