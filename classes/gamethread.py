import threading, os

class GameThread(threading.Thread):
	def __init__(self, game):
		threading.Thread.__init__(self)
	
		self.game = game

	def run(self):
		self.game.start()	
