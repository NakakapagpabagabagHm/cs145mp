import os, sys
import pygame 
import ConfigParser
from pygame.locals import *
from random import randint
from classes.ship import *
from classes.planet import *
from classes.UI import *
from classes.titleSprite import *

class Game:
	def __init__(self, screen):
		self.screen = screen
		
		self.bg = pygame.Surface((800, 600))
		self.bg = self.bg.convert()
		self.bg.fill((255,255,255))
		self.image = pygame.image.load('resources/background.png').convert_alpha()
		self.imageRect = self.image.get_rect()
		
		self.running = True

		self.group = pygame.sprite.Group()

		#A Ship
		self.a_ship = ship()
		self.group.add(self.a_ship)

		#B Ship
		self.b_ship = ship(2, 500, (0,255,0))
		self.b_ship.changeXY(self.b_ship.x, 500)
		self.b_ship.instance = 2
		self.b_ship.name = ">-xXx*/*pEiN*tHe*GrEaT*\*xXx-<"
		self.group.add(self.b_ship)

		self.ships = []
		self.ships.append(self.a_ship)
		self.ships.append(self.b_ship)

		self.planets = []
		self.orbits = []
		self.orbitGroup = pygame.sprite.Group()
		self.generatePlanets()

		self.bullets = []
		self.bulletGroup = pygame.sprite.Group()

		self.timer = timer()
		self.group.add(self.timer)

		self.killMessages = killMessages()

		self.resetControls()
		self.resetBControls()

		#Debug Key
		self.k_g = False

	def checkEvents(self):
		for event in pygame.event.get():
			if event.type == QUIT:
				self.running = False
				return

			if event.type == KEYDOWN:
				if event.key == K_ESCAPE:
					self.running = False
					return

				# A Ship Controls
				# w to accelerate
				if event.key == K_w:
					self.k_w = True
				else:
					self.k_w = False

				# s to deccelerate
				if event.key == K_s:
					self.k_s = True
				else:
					self.k_s = False 

				# q to rotate counterclockwise
				if event.key == K_q:
					self.k_q = True

				# e to rotate clockwise
				if event.key == K_e:
					self.k_e = True

				# f to shoot
				if event.key == K_f:
					self.k_f = True

				# B Ship Controls
				# i to accelerate
				if event.key == K_i:
					self.k_i = True
				else:
					self.k_i = False

				# k to deccelerate
				if event.key == K_j:
					self.k_j = True
				else:
					self.k_j = False

				# j to rotate counterclockwise
				if event.key == K_k:
					self.k_k = True
				else:
					self.k_k = False

				# l to rotate clockwise
				if event.key == K_l:
					self.k_l = True
				else:
					self.k_l = False

				if event.key == K_g:
					self.k_g = True
				else:
					self.k_g = False

			if event.type == KEYUP:
				if event.key == K_q:
					self.k_q = False

				if event.key == K_e:
					self.k_e = False

				if event.key == K_j:
					self.k_j = False

				if event.key == K_l:
					self.k_l = False

				if event.key == K_g:
					self.k_g = False

				if event.key == K_f:
					self.k_f = False
		
	def start(self):
		while(self.running):
			#Checks Events
			self.checkEvents()

			#Ship Movement
			self.a_ship.move(self.k_w, self.k_s, self.k_q, self.k_e)

			#B Ship Movement
			self.b_ship.move(self.k_i, self.k_k, self.k_j, self.k_l)

			#Ship Shooting
			self.a_ship.shoot(self.k_f, self.bulletGroup, self.bullets)

			#Collision
			collide_list = []
			for i in self.ships:
				#Planet Collision
				for j in self.planets:
					if pygame.sprite.collide_circle(i, j) and not i.isInvul:
						i.collide(j)
						self.resetControls()
						newMessage = suicideMessage(i)
						self.killMessages.addMessage(newMessage)
						self.group.add(newMessage)

				#Ship Collision
				for j in self.ships:
					if i.instance != j.instance and pygame.sprite.collide_circle(i,j) and not i.isInvul and not j.isInvul:
						i.collideShip()
						self.resetControls()
						
						if collide_list.count(j) > 0:
							newMessage = message(i,j)
							self.killMessages.addMessage(newMessage)
							self.group.add(newMessage)

						
						collide_list.append(i)
						collide_list.append(j)

				#Bullet Collision
				for j in self.bullets:
					if pygame.sprite.collide_circle(i, j) and i != j.ownership and i.isAlive and not i.isInvul:
						self.bullets.remove(j)
						j.die()
						i.collideShip()
						
						newMessage = message(i, j.ownership)
						self.killMessages.addMessage(newMessage)
						self.group.add(newMessage)

			#Bullet Collision with Ships
			for i in self.bullets:
				for j in self.planets:
					if pygame.sprite.collide_circle(i, j):
						self.bullets.remove(i)
						i.die()

			#Planet Orbits
			for i in self.planets:
				num_orbits = 0
				has_collided = False
				for j in self.ships:
					if pygame.sprite.collide_circle(i.orbit,j):
						has_collided = True
						num_orbits = num_orbits + 1

						#Clean Planet
						if not i.orbit.inOrbit:
							i.orbit.initOrbit(j)
						else:
							#Current Orbiting Ship is Orbiting it
							if i.orbit.shipInOrbit == j:
								pass
							else:
							#Different Ship is currently Orbiting it
								i.orbit.resetOrbit()
								i.orbit.initOrbit(j)

						#If more than one ship in Orbit, force reset orbit
						if num_orbits > 1:
							i.orbit.resetAllOrbit()

					elif i.orbit.shipInOrbit == j:
						i.orbit.resetOrbit()
				
				if not has_collided:
					i.orbit.drawOrbit(False)
				else:
					i.orbit.drawOrbit(True)
					
			#Debug
			if self.k_g:
				for i in self.planets:
					print i.name, ": in orbit =", i.orbit.inOrbit
					if i.orbit.shipInOrbit != None:
						print i.name, ":", i.orbit.shipInOrbit.name
					else:
						print i.name, ": None"				

			#Background
			self.screen.blit(self.bg, (0,0))
			self.screen.blit(self.image, self.imageRect)
			
			#Sprites
			self.killMessages.updateMessages()
			self.orbitGroup.update()		
			self.bulletGroup.update()	
			self.group.update()			
			self.orbitGroup.draw(self.screen)
			self.bulletGroup.draw(self.screen)
			self.group.draw(self.screen)	

			#Update Screen
			pygame.display.update()

	def resetControls(self):
		#Motion Keys
		self.k_w = False
		self.k_s = False
		self.k_q = False
		self.k_e = False
		self.k_f = False

	def resetBControls(self):
		#B Ship Motion Keys
		self.k_i = False
		self.k_j = False
		self.k_k = False
		self.k_l = False

	def generatePlanets(self):
		self.planetCount = 9
		
		#ringPlanet - 0
		#emberPlanet - 1
		#bluePlanet - 2
		#pinkPlanet - 3
		tempo_planets = []
		tries = 0

		while self.planetCount > len(tempo_planets):
			random_planet = randint(0,7)
			if random_planet == 0:
				tempo_planet = ringPlanet(randint(100, 700), randint(100, 500))
			elif random_planet == 1:
				tempo_planet = emberPlanet(randint(100, 700), randint(100, 500))
			elif random_planet == 2:
				tempo_planet = bluePlanet(randint(100, 700), randint(100, 500))		
			elif random_planet == 3:
				tempo_planet = pinkPlanet(randint(100, 700), randint(100, 500))	
			elif random_planet == 4:
				tempo_planet = earthPlanet(randint(100, 700), randint(100, 500))
			elif random_planet == 5:
				tempo_planet = marsPlanet(randint(100, 700), randint(100, 500))	
			elif random_planet == 6:
				tempo_planet = stationPlanet(randint(100, 700), randint(100, 500))	
			elif random_planet == 7:
				tempo_planet = mixPlanet(randint(100, 700), randint(100, 500))	
			tplanet_collide = False
			for i in tempo_planets:
				if pygame.sprite.collide_circle(i.s_orbit, tempo_planet.s_orbit):
					tplanet_collide = True

			if not tplanet_collide:
				tempo_planets.append(tempo_planet)
				tries = 0
			else:
				tries = tries + 1

			if tries == 1000:
				self.planetCount = self.planetCount - 1
				tries = 0

		for i in tempo_planets:
			self.planets.append(i)
			self.group.add(i)
			self.orbitGroup.add(i.orbit)
			self.orbitGroup.add(i.s_orbit)

class MainMenu:
	def __init__(self, screen):
		self.screen = screen

		self.bg = pygame.Surface((800, 600))
		self.bg = self.bg.convert()
		self.bg.fill((255,255,255))
		self.image = pygame.image.load('resources/background.png').convert_alpha()
		self.imageRect = self.image.get_rect()

		self.running = True

		self.spriteGroup = pygame.sprite.Group()
		self.titleSpriteGroup = pygame.sprite.Group()
		a_titleSprite = titleSprite()
		a_titleOrbit = titleOrbit(-150,250,1)
		b_titleOrbit = titleOrbit(900,250,-1)
		a_obstacle = titleFloat(-10,400)
		a_asteroid = titleFly()
		b_asteroid = titleFly()
		self.titleSpriteGroup.add(a_titleSprite)
		self.spriteGroup.add(a_titleOrbit)
		self.spriteGroup.add(b_titleOrbit)
		self.spriteGroup.add(a_obstacle)
		self.spriteGroup.add(a_asteroid)
		self.spriteGroup.add(b_asteroid)

		self.a_ship = ship(400-10,280)
		self.a_ship.mainMenuInit()
		self.spriteGroup.add(self.a_ship)
		self.resetControls()

		self.orbitSpriteGroup = pygame.sprite.Group()
		self.earthPlanet = earthPlanet(400-37,400)
		self.orbitSpriteGroup.add(self.earthPlanet.orbit)
		self.orbitSpriteGroup.add(self.earthPlanet.s_orbit)
		self.spriteGroup.add(self.earthPlanet)

	def checkEvents(self):
		for event in pygame.event.get():
			if event.type == QUIT:
				self.running = False
				return

			if event.type == KEYDOWN:
				if event.key == K_ESCAPE:
					self.running = False
					return

				# w to accelerate
				if event.key == K_w:
					self.k_w = True
				else:
					self.k_w = False

				# s to deccelerate
				if event.key == K_s:
					self.k_s = True
				else:
					self.k_s = False 

				# q to rotate counterclockwise
				if event.key == K_q:
					self.k_q = True

				# e to rotate clockwise
				if event.key == K_e:
					self.k_e = True

			if event.type == KEYUP:
				if event.key == K_q:
					self.k_q = False

				if event.key == K_e:
					self.k_e = False

	def start(self):
		while(self.running):
			#Check Events
			self.checkEvents()

			#Ship Movement
			self.a_ship.move(self.k_w, self.k_s, self.k_q, self.k_e)

			#Collision		
	
			#Earth Planet
			if(pygame.sprite.collide_circle(self.earthPlanet.orbit,self.a_ship)):
				if(self.earthPlanet.orbit.shipInOrbit != None):
					pass
				else:
					self.earthPlanet.orbit.initOrbit(self.a_ship)
				self.earthPlanet.orbit.drawOrbit(True)
			else:
				self.earthPlanet.orbit.drawOrbit(False)

			#Earth Planet Orbit
			if self.earthPlanet.ownership != None:
				self.running = False
				textmenu = TextMenu(self.screen)
				textmenu.start()

			#Background
			self.screen.blit(self.bg, (0,0))
			self.screen.blit(self.image, self.imageRect)
			
			#Updates
			self.titleSpriteGroup.update()
			self.spriteGroup.update()
			self.orbitSpriteGroup.update()

			#Render
			self.spriteGroup.draw(self.screen)
			self.titleSpriteGroup.draw(self.screen)
			self.orbitSpriteGroup.draw(self.screen)

			#Update Screen
			pygame.display.update()

	def resetControls(self):
		#Motion Keys
		self.k_w = False
		self.k_s = False
		self.k_q = False
		self.k_e = False

class TextMenu:
	def __init__(self, screen):
		self.caretPos = 1
		self.screen = screen

		self.bg = pygame.Surface((800, 600))
		self.bg = self.bg.convert()
		self.bg.fill((255,255,255))
		self.image = pygame.image.load('resources/mainmenu.png').convert_alpha()
		self.imageRect = self.image.get_rect()
		self.caret = pygame.image.load('resources/caret.png').convert_alpha()
		self.running = True
		self.spriteGroup = pygame.sprite.Group()
		self.titleSpriteGroup = pygame.sprite.Group()
		a_titleSprite = titleSprite()
		a_titleOrbit = titleOrbit(-150,250,1)
		b_titleOrbit = titleOrbit(900,250,-1)
		a_obstacle = titleFloat(-10,400)
		a_asteroid = titleFly()
		b_asteroid = titleFly()
		self.titleSpriteGroup.add(a_titleSprite)
		self.spriteGroup.add(a_titleOrbit)
		self.spriteGroup.add(b_titleOrbit)
		self.spriteGroup.add(a_obstacle)
		self.spriteGroup.add(a_asteroid)
		self.spriteGroup.add(b_asteroid)
		
		self.orbitSpriteGroup = pygame.sprite.Group()
		#set defaults here??
		self.ipAddress = '127.0.0.1'
		self.port = '8888'
		self.name = 'anonymous'
		self.skin = 'a'
		self.color = '255,255,255'
		
	def checkEvents(self):
		for event in pygame.event.get():
			if event.type == QUIT:
				self.running = False
				return

			if event.type == KEYDOWN:
				if event.key == K_DOWN:
					if self.caretPos < 4:
						self.caretPos += 1
					else:
						self.caretPos = 1
				if event.key == K_UP:
					if self.caretPos > 1:
						self.caretPos -= 1
					else:
						self.caretPos = 4
						
				if event.key == K_RETURN:
					if self.caretPos == 1:
						game = Game(self.screen)
						game.start()
					elif self.caretPos == 2:
						self.sett()
					elif self.caretPos == 3:
						self.help()
					elif self.caretPos == 4:
						self.running = False
						return

			# if event.type == KEYUP:
	# Help Menu
	def help(self):
		self.pimage = pygame.image.load('resources/help.png')
		self.pimageRect = self.pimage.get_rect()
		while(True):
			for event in pygame.event.get():
				if event.type == QUIT:
					self.running = False
					return
					
				if event.type == KEYDOWN:
					if event.key == K_ESCAPE:
						return
							
			self.checkEvents()
			self.screen.blit(self.pimage, self.pimageRect)
			pygame.display.flip()
	# Settings Menu
	def sett(self):
		config = ConfigParser.RawConfigParser()
		
		self.pimage = pygame.image.load('resources/settings.png')
		self.pimageRect = self.pimage.get_rect()
		self.hcaretPos = 1
		self.font = pygame.font.Font('resources/pixel.ttf',35)
		while(True):
			
			for event in pygame.event.get():
				if event.type == QUIT:
					self.running = False
					return
				if event.type == KEYDOWN:
					# print event.key
					if event.key == K_ESCAPE:
						config.add_section('Playerdata')
						config.set('Playerdata','IP',self.ipAddress)
						config.set('Playerdata','Port',self.port)
						config.set('Playerdata','Name',self.name)
						config.set('Playerdata','Skin',self.skin)
						config.set('Playerdata','Color',self.color)
						with open('settings.cfg','wb') as configfile:
							config.write(configfile)
						return				
						
					if event.key == K_DOWN:
						if self.hcaretPos < 5:
							self.hcaretPos += 1
						else:
							self.hcaretPos = 1
							
					if event.key == K_UP:
						if self.hcaretPos > 1:
							self.hcaretPos -= 1
						else:
							self.hcaretPos = 5
					
					if event.key == K_BACKSPACE:
						if self.hcaretPos == 1:
							self.ipAddress = self.ipAddress[:-1]
						elif self.hcaretPos == 2:
							self.port = self.port[:-1]
						elif self.hcaretPos == 3:
							self.name = self.name[:-1]
						elif self.hcaretPos == 4:
							self.skin = self.skin[:-1]
						elif self.hcaretPos == 5:				
							self.color = self.color[:-1]
							
					if event.key >= 97 and event.key <= 122 or event.key >= 48 and event.key <= 57 or event.key == 46 or event.key == 44:
						if self.hcaretPos == 1:
							self.ipAddress += chr(event.key)
						elif self.hcaretPos == 2:
							self.port += chr(event.key)
						elif self.hcaretPos == 3:
							self.name += chr(event.key)
						elif self.hcaretPos == 4:
							self.skin += chr(event.key)
						elif self.hcaretPos == 5:				
							self.color += chr(event.key)
						
			self.checkEvents()
			self.screen.blit(self.pimage, self.pimageRect)
			self.screen.blit(self.font.render(self.ipAddress,0,(255,255,255)),(150, 172))
			self.screen.blit(self.font.render(self.port,0,(255,255,255)),(190, 226))
			self.screen.blit(self.font.render(self.name,0,(255,255,255)),(215, 280))
			self.screen.blit(self.font.render(self.skin,0,(255,255,255)),(425, 388))
			self.screen.blit(self.font.render(self.color,0,(255,255,255)),(370, 442))
			
			if self.hcaretPos == 1:
				self.screen.blit(self.caret,(40,166))
			elif self.hcaretPos == 2:
				self.screen.blit(self.caret,(40,220))
			elif self.hcaretPos == 3:
				self.screen.blit(self.caret,(40,274))
			elif self.hcaretPos == 4:
				self.screen.blit(self.caret,(40,382))
			elif self.hcaretPos == 5:
				self.screen.blit(self.caret,(40,436))

			pygame.display.flip()
			
			
	# Start
	def start(self):
		while(self.running):
			#Check Events
			self.checkEvents()
			
			#Background
			self.screen.blit(self.bg, (0,0))
			self.screen.blit(self.image, self.imageRect)
			if self.caretPos == 1:
				self.screen.blit(self.caret,(23,353))
			elif self.caretPos == 2:
				self.screen.blit(self.caret,(58,407))
			elif self.caretPos == 3:
				self.screen.blit(self.caret,(95,461))
			elif self.caretPos == 4:
				self.screen.blit(self.caret,(131,515))
			#Updates
			self.titleSpriteGroup.update()
			self.spriteGroup.update()
			self.orbitSpriteGroup.update()
			
			#Render
			self.spriteGroup.draw(self.screen)
			self.titleSpriteGroup.draw(self.screen)
			self.orbitSpriteGroup.draw(self.screen)
			
			#Update Screen
			pygame.display.update()



if __name__ == '__main__':

	pygame.init()
	pygame.font.init()
	SCREEN = pygame.display.set_mode((800, 600))
	# mainmenu = MainMenu(SCREEN) 
	mainmenu = TextMenu(SCREEN)
	mainmenu.start()
	#game = Game(SCREEN)
	#game.start()
